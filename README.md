# Structural Clustering

A small package to cluster MD trajectories by structural similarities.

## Details

The package is utilizing KMedoids algorithm from [MSMBuilder](http://msmbuilder.org/development/) to cluster structures basing on RMSD. It can work on data from US simulation, computing reaction coordinate (distance between centers of masses of two groups of atoms for now) and applying boundaries to the values to sift trajectory. Before clustering it performs aligning of structures based on RMSD of given group.


## Dependencies

The package is dedicated for [GROMACS](http://www.gromacs.org/) users, consequently operates on its file formats (i.e. `xtc` trajectory files, `pdb` structure files and `ndx` index files). To perform basic trajectory operations [MDTraj package](http://mdtraj.org/1.9.3/) is used and for mathematic operations [numpy](https://numpy.org/) and [scipy](https://www.scipy.org/) are used.

## Installation

### Creating conda environment (optional)

The conda environments are IMO convenient, because you do not interfere with system python and are able to create one with all dependencies with just one command! Do the following:

    git clone https://gitlab.com/KomBioMol/structural_clustering.git
    cd structural_clustering
    conda env create -f environment.yml 
    conda activate structural_clustering

Then proceed with installation the package:

    pip install .

### Pip

You can skip previous step and just install requirements and the package with pip:

    git clone https://gitlab.com/KomBioMol/structural_clustering.git
    cd structural_clustering
    pip install -r requirements.txt
    pip install .

## Usage

Once installed you have access to script `structural_clustering`. Feel free to run `structural_clustering -h` to see all options.

### Metadata file

The main input file is so called metadata.dat (flag `-m`). It consists of four columns:
    
    traj0.xtc 1.0 238.846 0.0
    traj0.xtc 1.1 238.846 1.0
    traj0.xtc 1.2 238.846 2.0

The first column contains paths to US trajectories, second one -- center of harmonic potetnial, third one -- force constants (unit: kcal/mol !!!) and last one -- F coefficient from WHAM.

### Trajectories preparation

It is good idea to prepare trajectories before running this program. First of all, to avoid pdb problems, the following operations are recommended (performed using GROMACS):

1. combine parts together:

    `gmx trjcat -f sim.part*xtc -o sim.xtc`
    
2. make molecules whole through pbc:

    `gmx trjconv -f sim.xtc -s sim0.tpr -o whole.xtc -pbc whole`
    
    Output: *System*

3. cluster all the atoms in the selected index such that they are all closest to the center of mass of the cluster, which is iteratively updated (it is different type of clustering): 

    `gmx trjconv -f whole.xtc -s sim0.tpr -n index.ndx -o clus.xtc -pbc cluster`

    Clustering: e.g. *Protein\_Ligand*, Output: *System*

4. do other fancy stuff

    `gmx trjconv -f clus.xtc -s sim0.tpr -n index.ndx -o mol.xtc -pbc mol -center -ur compact`

    Centering: e.g. *Protein\_Ligand*, Output: *System*

Additionally one can remove water molecules to decrease size of files and thus avoid RAM overloading problems. Just replace word *System* in output with *non-Water* etc.

### Basic input

Except metafile, the basic input files are: structure file (`pdb` with flag `-s`) and index file (`ndx` with flag `-n`). They are required to run the program.

### Reaction coordinate

In current version the only reaction coordinate supported is distance between centers of mass of two groups (provided by flag `-r`). They have to be present in index file.

### Sifting data

Usually one does not want to include frames from whole range of reaction coordinate, but from a small region. To introduse boundaries and sift trajectories, use flags `-l` for lower boundary and `-u` for upper one. 

### Aligning structures

In each frame of trajectory molecules are oriented more or less randomly. To align them on part of the system, use option `-a`. Without it further clustering can make no sense, because RMSD distances may be results of the random orientation of molecules, not their structural differences!

### Choosing group for clustering

Choose the group of atoms with flag `-t` to perform clustering, using them to compute RMSD distances. The group should not be too large, because RAM overloading may occur. It is recommended to use heavy atoms of some particular molecules etc.

### Elbow method

To figure out how many clusters are present in given system, [elbow method](https://en.wikipedia.org/wiki/Elbow_method_(clustering)) becomes useful. To run these calculations provide flag `-k` with `0`. Program will run clustering procedure with number of cluster varying from 1 to 10 and return standard squared error of assignment. The results are saved to file `elbow.dat`.

### Actual clustering

Once all preparations are made and number of clusters is determine, to perform actual clustering, provide flag `-k` with proper number. The following files are saved: structures of centers (files `center_k.pdb`), each of trajectory splited into clusters (with proper suffixes) and weighted population of each cluster (file `population.dat`)

## Licence

The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND!

## Contribution

We would appreciate any comments, advises and help with developing and maintaining this package.
