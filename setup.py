from setuptools import setup, find_packages

setup(
        name="structural_clustering",
        version="0.1",
        description="Clustering MD trajectories by structural similarities.",
        author="Cyprian Kleist",
        author_email="Cyp2505@wp.pl",
        packages=find_packages(),
        scripts=["./bin/structural_clustering"]
    )

