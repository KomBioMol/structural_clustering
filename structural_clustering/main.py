from structural_clustering.utils import parse, Noise
from structural_clustering.weighting import UmbrellaSampling
from structural_clustering.ndx import Ndx
from structural_clustering.clustering import Clustering

from msmbuilder.cluster import KMedoids

def main():
    args = parse()
    noise = Noise(args.verbose)

    noise("Reading ndx file...")
    n = Ndx(args.ndx)
    noise("Done")

    noise("Loading US data...")
    u = UmbrellaSampling(args.metafile, args.pdb)
    noise("Done")

    noise("Computing rcoord...")
    u.compute_rcoord(n[args.rcoord_grps[0]], n[args.rcoord_grps[1]])
    noise("Done")

    noise("Computing weights...")
    u.compute_weights()
    noise("Done")

    if args.k == 0:
        noise("You chose to check elbow plot. Computing SSE for number of clusters from 1 to 10...")
        sse = {}
        for i in range(1,11):
            noise(f"\n### Iteration: {i}... ###\n")
            c = Clustering(KMedoids, i)
            c.set_data_us(u)
            noise(f"Sifting trajectories with bounds - lower: {args.lower} and upper: {args.upper}...")
            if args.lower is not None and args.upper is not None:
                c.sift_trajs(args.lower, args.upper)
            elif args.lower is not None:
                c.sift_trajs(lower=args.lower)
            elif args.upper is not None:
                c.sift_trajs(upper=args.upper)
            else:
                c.sift_trajs()
            noise("Done")
            if args.align_grp is not None:
                noise(f"Aligning structure - chosen group: {args.align_grp}...")
                c.align_RMSD(n[args.align_grp])
                noise("Done")
            noise(f"Clustering - chosen group: {args.fit_grp}...")
            c.fit(n[args.fit_grp])
            noise("Done")
            s = c.SSE()
            sse[i] = s
            noise(f"\n### SSE for performed clustering: {s:.2f} ###\n")
            noise("\n### Iteration finished ###\n")

        noise("\n### SSE of clusters: ###\n")
        for key, val in sse.items():
            noise(f"{key:2} --- {val:.2f}")
        noise("\n#################*#######\n")
        noise("Saving results to file: elbow.dat...")
        with open("elbow.dat", 'w') as f:
            for key, val in sse.items():
                f.write(f"{key:2} --- {val:.2f}\n")
        noise("\n### Weighted population of clusters: ###\n")
        noise("Done")
        noise("Finished successfully!")


    else:
        noise(f"You chose {args.k} number of clusters. Proceeding with clustering...")
        c = Clustering(KMedoids, args.k)
        c.set_data_us(u)

        noise(f"Sifting trajectories with bounds - lower: {args.lower} and upper: {args.upper}...")
        if args.lower is not None and args.upper is not None:
            c.sift_trajs(args.lower, args.upper)
        elif args.lower is not None:
            c.sift_trajs(lower=args.lower)
        elif args.upper is not None:
            c.sift_trajs(upper=args.upper)
        else:
            c.sift_trajs()
        noise("Done")

        if args.align_grp is not None:
            noise(f"Aligning structure - chosen group: {args.align_grp}...")
            c.align_RMSD(n[args.align_grp])
            noise("Done")

        noise(f"Clustering - chosen group: {args.fit_grp}...")
        c.fit(n[args.fit_grp])
        noise("Done")

        # noise(f"\n### SSE for performed clustering: {c.SSE():.2f} ###\n")

        noise("Splitting trajectories...")
        c.split2clusters()
        noise("Done")

        noise("Saving centers of clusters...")
        c.save_centers()
        noise("Done")

        popul = c.compute_population()
        with open("population.dat", 'w') as f:
            for key, val in popul.items():
                f.write(f"{key:2} --- {100*val:.2f}%\n")
        noise("\n### Weighted population of clusters: ###\n")
        for key, val in popul.items():
            noise(f"       {key:2} --- {100*val:.2f}%")
        noise("\n########################################\n")
        noise("Finished successfully!")

if __name__ == "__main__":
    main()
