import argparse as arg


def parse():
    description = "Structural clustering."
    parser = arg.ArgumentParser(description=description)
    parser.add_argument(
            "-v",
            "--verbose",
            dest="verbose",
            action="store_true",
            help="be noisy")
    parser.add_argument(
            "-m",
            metavar="metafile.dat",
            type=str,
            nargs='?',
            dest="metafile",
            help="file with trajs list and us parameters")
    parser.add_argument(
            "-s",
            metavar="structure.pdb",
            type=str,
            nargs='?',
            dest="pdb",
            help="structure file")
    parser.add_argument(
            "-n",
            metavar="index.ndx",
            type=str,
            nargs='?',
            dest="ndx",
            help="index file")
    parser.add_argument(
            "-k",
            metavar='k',
            type=int,
            nargs='?',
            dest="k",
            help="number of clusters, 0 for producing elbow plot")
    parser.add_argument(
            "-r",
            metavar='group',
            type=str,
            nargs=2,
            dest="rcoord_grps",
            help="two groups to compute rcoord (distance between CoMs)")
    parser.add_argument(
            "-a",
            metavar='group',
            type=str,
            nargs='?',
            dest="align_grp",
            help="group to align RMSD during clustering")
    parser.add_argument(
            "-t",
            metavar='group',
            type=str,
            nargs='?',
            dest="fit_grp",
            help="group to perform clustering on")
    parser.add_argument(
            "-l",
            metavar='rc',
            type=float,
            nargs='?',
            dest="lower",
            help="lower bound for rcoord for clustering")
    parser.add_argument(
            "-u",
            metavar='rc',
            type=float,
            nargs='?',
            dest="upper",
            help="upper bound for rcoord for clustering")
    args = parser.parse_args()
    return args


class Noise():

    def __init__(self, verbose):
        self.verbose = verbose

    def __call__(self, com, **kwargs):
        if self.verbose:
            print(com, **kwargs)
        else:
            pass


