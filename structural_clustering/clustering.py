import mdtraj as md
import numpy as np


class Clustering(object):

    def __init__(self, algo, n_clusters):
        self.algorithm = algo(n_clusters, metric='rmsd')
        self.n_clusters = n_clusters
        self.us = None
        self.trajs = None
        self.frames = []
        self.weights = []
        self.clustering_group = 'all'

    def set_data_us(self, us):
        self.us = us

    def sift_trajs(self, lower=-np.inf, upper=np.inf, save=False):
        for traj, rc, weight in zip(self.us.trajs, self.us.rcs, self.us.weights):
            inds = np.where((lower<rc) & (rc<upper))
            self.frames.append(inds[0])
            xtc = md.load_xtc(traj, top=self.us.top)
            if self.trajs is None:
                self.trajs = xtc[inds]
            else:
                self.trajs = self.trajs.join(xtc[inds])
            self.weights.append(weight[inds])
            if save:
                xtc[inds].save_xtc(f"{traj.split('.')[0]}_sifted.xtc")
                np.save(f"{traj.split('.')[0]}_sifted.npy", weight[inds])

    @staticmethod
    def combine_trajs(trajs):
        wholetraj = trajs[0]
        if len(trajs) > 1:
            wholetraj = wholetraj.join(trajs[1:])
        return wholetraj

    def align_RMSD(self, group, ref=None):
        if ref is None:
            ref = self.trajs[0]
        self.trajs = self.trajs.superpose(ref, atom_indices=group)

    def fit(self, group=None):
        if group is not None:
            self.clustering_group = group
            wholetraj = self.trajs.atom_slice(group)
        else:
            wholetraj = self.trajs
        self.algorithm.fit(wholetraj)

    def split2clusters(self):
        b = 0
        e = 0
        preds = np.array(self.algorithm.labels_).flatten()
        for i, frame in enumerate(self.frames):
            b = int(e)
            e += len(frame)
            pred = preds[b:e]
            traj = self.trajs[b:e]
            fnm = '.'.join(self.us.trajs[i].split('.')[:-1])
            for k in range(self.n_clusters):
                if len(np.where(pred==k)[0]) > 0:
                    traj[pred==k].save_xtc(f"{fnm}_cluster{k}.xtc")

    def save_centers(self):
        if hasattr(self.algorithm, 'cluster_centers_'):
            centers = self.algorithm.cluster_centers_
        else:
            centers = []
        for i, c in enumerate(centers):
            c.save_pdb(f"center_{i}.pdb")

    def SSE(self):
        if hasattr(self.algorithm, "distances_"):
            distances = self.algorithm.distances_
        else:
            distances = self.dists2centers()
        return np.sum(np.square(distances))

    def dists2centers(self):
        distances = []
        if hasattr(self.algorithm, 'cluster_centers_'):
            centers = self.algorithm.cluster_centers_
        else:
            centers = []
        if self.clustering_group != 'all':
            traj = self.trajs.atom_slice(self.clustering_group)
        else:
            traj = self.trajs
        preds = np.array(self.algorithm.labels_).flatten()
        for frame, center in zip(traj, preds):
            distances.append(md.rmsd(frame, centers[center]))
        return np.array(distances).flatten()

    def compute_population(self):
        weights = np.hstack(self.weights)
        preds = np.array(self.algorithm.labels_).flatten()
        pop = {}
        for i in range(self.n_clusters):
            pop[i] = np.sum(weights[preds==i]) / weights.sum()
        return pop


def tmp(k):
    c = Clustering(KMedoids, k)
    c.set_data_us(u)
    c.sift_trajs()
    c.align_RMSD([415,418,419,423,428,448,451,452,456,461,481,484,485,489,494,546,549,550,554,559,579,582,583,587,592,612,615,616,620,625,709,712,713,717,722,742,745,746,750,755,775,778,779,783,788,840,843,844,848,853,873,876,877,881,886,906,909,910,914,919])
    c.fit()
    return c.dists2centers()

