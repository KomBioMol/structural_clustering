import mdtraj as md
import numpy as np

class UmbrellaSampling(object):

    """Docstring for UmbrellaSampling. """


    def __init__(self, fnm, top):
        """TODO: to be defined. """
        self.metafile = self.__parse_input_file(fnm)
        self.top = top
        self.trajs = [ i[0] for i in self.metafile ]
        self.ds = [ i[1] for i in self.metafile ]
        self.ks = [ i[2] for i in self.metafile ]
        self.fs = [ i[3] for i in self.metafile ]
        self.rcs = []
        self.weights = []

    def compute_rcoord(self, group1, group2):
        for traj in self.trajs:
            rcoord = np.empty((0,))
            for chunk in md.iterload(traj, top=self.top, chunk=5000):
                rcoord = np.append(rcoord, self.__com_distance(chunk, group1, group2))
            self.rcs.append(rcoord)

    def compute_weights(self, T=300):
        for rc, d, k, f in zip(self.rcs, self.ds, self.ks, self.fs):
            self.weights.append(self.__weight(rc, d, k, f, T))


    @staticmethod
    def __parse_input_file(fnm):
        data = None
        with open(fnm) as f:
            data = f.readlines()
        data = [ d.strip('\n').split() for d in data ]
        for i in range(len(data)):
            for j in range(1, len(data[i])):
                data[i][j] = float(data[i][j])
        return data

    @staticmethod
    def __weight(rc, d, k, f, T):
        KB = 0.001987  # Boltzmann's constant [kcal/molK]
        weights = np.exp(-(f - 0.5*k*np.power((rc - d),2))/(KB*T))
        return weights # / np.sum(weights)

    @staticmethod
    def __com_distance(traj, group1, group2):
        """Compute distance between center of mass of two groups

        Parameters
        ----------
        traj : TODO
        group1 : TODO
        group2l : TODO Returns
        -------
        TODO

        """
        com1 = md.compute_center_of_mass(traj.atom_slice(group1))
        com2 = md.compute_center_of_mass(traj.atom_slice(group2))
        return np.linalg.norm(com2 - com1, axis=1)
