import mdtraj as md
import numpy as np


def com_distance(traj, group1, group2):
    """Compute distance between center of mass of two groups

    Parameters
    ----------
    traj : TODO
    group1 : TODO
    group2l : TODO

    Returns
    -------
    TODO

    """
    com1 = md.compute_center_of_mass(traj.atom_slice(group1))
    com2 = md.compute_center_of_mass(traj.atom_slice(group2))
    return np.linalg.norm(com2 - com1, axis=1)


def from_file(fnm, column=1):
    """Load rcoord from file

    Parameters
    ----------
    fnm : TODO

    Returns
    -------
    TODO

    """
    return np.loadtxt(fnm, usecols=(column,))
